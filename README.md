# Go client

##nginx config

    server {
        listen       80;
        server_name  _;

        root   /var/go-project/go-client/public/main-dev/;
        index  index.html index.htm;

        location / {
            if (!-e $request_filename){
                rewrite ^(.*)$ /index.html break;
            }
        }
    }


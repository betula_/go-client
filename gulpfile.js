var gulp = require('gulp');
var gutil = require('gulp-util');
var concat = require('gulp-concat');
var argv = require('minimist')(process.argv);
var path = require('path');
var through = require('through2');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var connect = require('gulp-connect');
var clean = require('gulp-clean');

function plumber(options) {
    options = Object(options) || {};
    options.errorHandler = function(err) {
        if (!(err instanceof Error)) {
            err = new Error(err);
        }
        console.error(gutil.colors.red(err.message));
    };
    return require('gulp-plumber')(options);
}


function getAppName() {
    return argv.app || 'mobile';
}

function getEnvType() {
    return argv.env || 'dev';
}

function getPublicPath() {
    return path.join('public', getAppName(), getEnvType());
}

function getSrcPath() {
    return 'src';
}

function isJsDebug() {
    return argv['js-debug'];
}

function isWatch() {
    return argv.watch;
}

gulp.task('clean', function() {
    return gulp.src(path.join(getPublicPath(), '**/*'), {read: false})
        .pipe(plumber())
        .pipe(clean());
});

var res = ['fonts', 'images', 'partials', 'vendors'];
res.forEach(function(name) {
    gulp.task(name, function() {
        return gulp.src([path.join(name, '**')], {cwd: getSrcPath(), base: getSrcPath()})
            .pipe(plumber())
            .pipe(gulp.dest('', {cwd: getPublicPath()}));
    });
});

gulp.task('resources', res);

gulp.task('scripts', function() {
    return gulp.src(['scripts/*/*'], {cwd: getSrcPath(), base: getSrcPath()})
        .pipe(plumber())
        .pipe(through.obj(function(file, enc, next) {
            var files = ['module.prefix', 'module.js', '**/*.js', 'module.suffix'];
            if (!isJsDebug()) {
                files.push('!debug/**');
            }

            gulp.src([path.join(file.relative, '**/*.js')], {cwd: getSrcPath(), base: getSrcPath()})
                .pipe(plumber())
                .pipe(concat(path.basename(file.path)+'.js'))
                .pipe(gulp.dest(path.dirname(file.relative), {cwd: getPublicPath()}));
            next();
        }));
});

gulp.task('styles', function() {
    return gulp.src(['styles/**/*.scss', '!styles/**/_*.scss'], {cwd: getSrcPath(), base: getSrcPath()})
        .pipe(plumber())
        .pipe(sass())
        .pipe(autoprefixer('> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1', 'Explorer 9', 'iOs 6'))
        .pipe(gulp.dest('', {cwd: getPublicPath()}));
});

gulp.task('indexes', function() {
    return gulp.src(path.join('indexes', getAppName(), '**'), {cwd: getSrcPath()})
        .pipe(plumber())
        .pipe(gulp.dest('', {cwd: getPublicPath()}));
});

gulp.task('watch', function() {
    ['fonts', 'images', 'partials', 'vendors', 'scripts', 'styles', 'indexes'].forEach(function(name) {
        gulp.watch([path.join(name, '**')], {cwd: getSrcPath()}, [name]);
    });
});

gulp.task('connect', function() {
    connect.server({
        root: getPublicPath(),
        port: 8000
    });
});


gulp.task('build', ['resources', 'scripts', 'styles', 'indexes'].concat( isWatch() ? ['connect', 'watch'] : [] ));
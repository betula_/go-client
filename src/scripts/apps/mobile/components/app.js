module.directive('app', [function() {
    return {
        restrict: 'E',
        templateUrl: 'partials/apps/mobile/component/app.html'
    }
}]);
'use strict';

var module = angular.module('app.mobile', ['ionic', 'module.extensions', 'module.common', 'ngCookies']);

module.run(['$ionicPlatform', function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        if(window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
}]);

module.config(['$urlRouterProvider', function($urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
}]);
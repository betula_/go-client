

module.config(['$stateProvider', function($stateProvider) {
    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: 'partials/apps/mobile/page/home.html'
        });
}]);
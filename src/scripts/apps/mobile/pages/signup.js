

module.config(['$stateProvider', function($stateProvider) {
    $stateProvider
        .state('signup', {
            url: '/signup',
            templateUrl: 'partials/apps/mobile/page/signup.html'
        });
}]);
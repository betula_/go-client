
module.directive('bidsPanel', [function () {
  return {
    restrict: 'E',
    scope: {},
    templateUrl: '/template/main/component/bids/bidsPanel.html',
    controller: ['$scope', '$api', '$timeout', '$session', '$location', function($scope, $api, $timeout, $session, $location) {

      var bids = {
        myBid: null,
        items: [],
        performed: false,

        search: function() {
          this.myBid = null;
          //this.items = [];
          this.performed = false;

          var self = this;
          $api.bids().$promise.then(function(data) {
            self.items = data;
            data.forEach(function(bid) {
              if (bid.user.id == $session.user.id) {
                bids.myBid = bid;
              }
            });
            self.performed = true;
          });
        },

        accept: function(bid) {
          $api.game(bid.id).$promise.then(function() {
            bid.accepted = true;
            $location.url('/game');
          });
        },

        create: function() {
          $api.bid().$promise.then(function() {
            bids.search();
          });
        }

      };

      $scope.bids = bids;

      $timeout(function() {
        bids.search();
      }, 100);

      var timer = setInterval(function() {
        $scope.$apply(function() {
          bids.search();
        });
      }, 3000);

      $scope.$on('$destroy', function() {
        clearInterval(timer);
      });

    }]
  };
}]);
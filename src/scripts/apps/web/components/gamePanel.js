
module.directive('gamePanel', [function () {
  return {
    restrict: 'E',
    scope: {},
    templateUrl: '/template/main/component/game/gamePanel.html',
    controller: ['$scope', '$api', '$timeout', '$session', '$location', function($scope, $api, $timeout, $session, $location) {

      this.user = $session.user;

      this.board = [];
      this.game = null;
      this.player = null;
      this.opponent = null;

      this.refresh = function() {
        var self = this;
        $api.game.active().$promise.then(function(game) {
          self.game = game;
          self.redraw();
        }, function() {
          $location.url('/games');
          self.game = null;
          self.redraw();
        });
      };

      this.redraw = function() {
        if (!this.game) {
          this.board = [];
          this.player = null;
          this.opponent = null;
          return;
        }

        var game = this.game;
        game.gamers.forEach(function(gamer) {
          if (gamer.user.id == self.user.id) {
            self.player = gamer;
            self.player.deaths = [];
          }
          else {
            self.opponent = gamer;
            self.opponent.deaths = [];
          }
        });


        var board = [];
        var i, j;
        for (i = 0; i < game.size; i++) {
          board[i] = [];
          for (j = 0; j < game.size; j++) {
            board[i][j] = {
              row: i,
              col: j
            };
          }
        }

        game.steps.forEach(function(step) {
          var stone = step.stone;
          if (stone) {
            if (!stone.isdeath) {
              board[stone.row][stone.col].stone = stone;
            }
            else {
              if (step.user.id == self.player.user.id) {
                self.player.deaths.push(stone);
              }
              else {
                self.opponent.deaths.push(stone);
              }
            }
          }
        });

        this.board = board;

      };

      this.step = function(cell) {
        if (!this.isMyStep()) return;
        if (cell.stone) return;

        var self = this;
        $api.game.active.step(cell.row, cell.col).$promise.then(function() {
          self.refresh();
        });
      };


      this.isMyStep = function() {
        if (!this.player) {
          return false;
        }
        if (this.game.isclosed) {
          return false;
        }
        var last = this.game.steps.slice(-1)[0];
        if (!last) {
          return this.player.iswhite;
        }
        return last.user.id != this.player.user.id;
      };

      this.pass = function() {
        if (!this.isMyStep()) return;
        var self = this;
        $api.game.active.step.pass().$promise.then(function() {
          self.refresh();
        });
      };

      this.quit = function() {
        var self = this;
        $api.game.active.quit().$promise.then(function() {
          self.refresh();
        });
      };


      var self = this;
      $timeout(function() {
        self.refresh();
      }, 100);

      var timer = setInterval(function() {
        $scope.$apply(function() {
          self.refresh();
        });
      }, 3000);

      $scope.$on('$destroy', function() {
        clearInterval(timer);
      });


      $scope.game = this;

    }]
  };
}]);
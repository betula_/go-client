
module.directive('gamesPanel', [function () {
  return {
    restrict: 'E',
    scope: {},
    templateUrl: '/template/main/component/games/gamesPanel.html',
    controller: ['$scope', '$api', '$timeout', '$session', function($scope, $api, $timeout, $session) {

      var games = {
        items: [],
        user: $session.user,

        search: function() {
          this.items = $api.games();

          var self = this;
          this.items.$promise.then(function(games) {
            games.forEach(function(game) {
              if (game.gamers[0].point > game.gamers[1].point) {
                game.gamers[0].winner = true;
                game.gamers[1].loser = true;
                if (game.gamers[0].user.id == self.user.id) {
                  game.win = true;
                }
                else {
                  game.lose = true;
                }
              }
              if (game.gamers[0].point < game.gamers[1].point) {
                game.gamers[0].loser = true;
                game.gamers[1].winner = true;
                if (game.gamers[0].user.id == self.user.id) {
                  game.lose = true;
                }
                else {
                  game.win = true;
                }
              }
            });
          });
        }

      };

      $scope.games = games;

      $scope.timeAgo = function(date) {
        return moment(date).fromNow();
      };

      games.search();

    }]
  };
}]);

module.directive('header', [function () {
  return {
    restrict: 'E',
    scope: {},
    templateUrl: '/template/main/component/header/header.html',
    controller: ['$scope', '$session', '$location', function($scope, $session, $location) {

      $scope.pathStartWith = function(path) {
        return path == $location.path().substr(0, path.length);
      };

      $scope.session = $session;
      $scope.signout = function() {
        $session.destroy();
        $location.url('/');
      };
    }]
  };
}]);
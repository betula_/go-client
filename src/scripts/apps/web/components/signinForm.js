module.directive('signinForm', [function () {
  return {
    restrict: 'AE',
    scope: {},
    templateUrl: '/template/main/component/signin/signinForm.html',
    controller: ['$scope', '$session', '$location', function($scope, $session, $location) {

      var model = $scope.model = {};

      model.email = 'a@x.com';

      $scope.signin = function() {
        $scope.userNotFound = false;
        if (this.form.$invalid) {
          return;
        }

        $session.start(model.email, model.password).then(function(session) {
          if (session.authorized && session.roles.isUser) {
            $location.url('/bids');
            return;
          }
          $scope.userNotFound = true;
        }, function() {
          $scope.userNotFound = true;
        });

      }

    }]
  };
}]);
module.directive('signupForm', [function () {
  return {
    restrict: 'AE',
    scope: {},
    templateUrl: '/template/main/component/signup/signupForm.html',
    controller: ['$scope', '$session', '$location', '$api', function($scope, $session, $location, $api) {

      var model = $scope.model = {};

      $scope.signup = function() {
        $scope.userConflict = false;
        if (this.form.$invalid) {
          return;
        }

        $api.user(model.email, model.password).$promise.then(function() {

          $session.start(model.email, model.password).then(function(session) {
            if (session.authorized && session.roles.isUser) {
              $location.url('/bids');
              return;
            }
            $scope.userConflict = true;
          }, function() {
            $scope.userConflict = true;
          });

        }, function() {
          $scope.userConflict = true;
        });


      }

    }]
  };
}]);
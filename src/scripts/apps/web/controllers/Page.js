
module.controller('Page', ['$scope', '$page', '$session', '$api', '$location', function($scope, $page, $session, $api, $location) {

  var checkGame = function() {
    $session.perform().then(function(sess) {
      if (!sess.roles.isUser) return;
      $api.game.active().$promise.then(function() {
        $location.url('/game');
      });
    });
  };

  var timer = setInterval(function() {
    checkGame();
  }, 5000);

  $scope.$on('$destroy', function() {
    clearInterval(timer);
  });



}]);
'use strict';

var module = angular.module('app.main', ['Common', 'ngCookies', 'ngRoute']);

module.config(['$locationProvider', function($locationProvider) {
  $locationProvider.html5Mode(true);
}]);




module.config(['$routeProvider', function($routeProvider) {

  $routeProvider.when('/game', {
    templateUrl: '/template/main/page/game.html',
    resolve: {
      game: ['$session', '$q', '$location', '$api', function($session, $q, $location, $api) {
        var done = $q.defer();
        $session.perform().then(function(sess) {
          if (!sess.roles.isUser) {
            done.reject();
            $location.url('/');
            return;
          }

          $api.game.active().$promise.then(function(game) {
            done.resolve(game);
          }, function() {
            done.reject();
            $location.url('/');
          });

        });
        return done.promise;
      }]
    },
    controller: ['game', '$scope', function(game, $scope) {
      $scope.game = game;
    }]
  });

}]);


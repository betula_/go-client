
module.config(['$routeProvider', function($routeProvider) {

  $routeProvider.when('/games', {
    templateUrl: '/template/main/page/games.html',
    resolve: {
      session: ['$session', '$q', '$location', function($session, $q, $location) {
        var done = $q.defer();
        $session.perform().then(function(sess) {
          if (!sess.roles.isUser) {
            done.reject();
            $location.url('/');
            return;
          }
          done.resolve(sess);
        });
        return done.promise;
      }],
      game: ['$session', '$q', '$location', '$api', function($session, $q, $location, $api) {
        var done = $q.defer();
        $session.perform().then(function(sess) {
          if (!sess.roles.isUser) {
            done.resolve();
            return;
          }
          $api.game.active().$promise.then(function() {
            done.reject();
            $location.url('/game');
          }, function() {
            done.resolve();
          });
        });
        return done.promise;
      }]
    }
  });

}]);


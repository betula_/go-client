
module.config(['$routeProvider', function($routeProvider) {

  $routeProvider.when('/', {
    templateUrl: '/template/main/page/home.html',
    controller: 'HomePage',
    resolve: {
      session: ['$session', function($session) {
        return $session.perform();
      }],
      game: ['$session', '$q', '$location', '$api', function($session, $q, $location, $api) {
        var done = $q.defer();
        $session.perform().then(function(sess) {
          if (!sess.roles.isUser) {
            done.resolve();
            return;
          }
          $api.game.active().$promise.then(function() {
            done.reject();
            $location.url('/game');
          }, function() {
            done.resolve();
          });
        });
        return done.promise;
      }]
    }
  });

  $routeProvider.otherwise({
    redirectTo: '/'
  });

}]);



module.config(['$routeProvider', function($routeProvider) {

  $routeProvider.when('/signin', {
    templateUrl: '/template/main/page/signin.html',
    resolve: {
      session: ['$session', '$q', '$location', function($session, $q, $location) {
        var done = $q.defer();
        $session.perform().then(function(sess) {
          if (sess.roles.isUser) {
            done.reject();
            $location.url('/');
            return;
          }
          done.resolve(sess);
        });
        return done.promise;
      }]
    }
  });

}]);


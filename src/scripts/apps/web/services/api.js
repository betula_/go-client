
module.factory('$api', ['$http', '$sessionStore', '$config', '$loadingIndicator', function($http, $sessionStore, $config, $loadingIndicator) {
  var extend = angular.extend;

  var api = function(config) {
    var session = $sessionStore.get();
    if (session) {
      config.headers = extend({
        Authorization: session.token
      }, config.headers || {});
    }
    $loadingIndicator.start();
    var promise = $http(config);
    promise.then($loadingIndicator.stop, function(error) {
      if (error.status == 401) {
        $sessionStore.clear();
      }
      $loadingIndicator.stop();
    });
    return promise;
  };
  api.config = $config.api;

  ['get', 'delete', 'head', 'jsonp'].forEach(function(name) {
    api[name] = function(url, config) {
      return api(extend(config || {}, {
        method: name,
        url: api.config.host + url
      }));
    };
  });

  ['post', 'put'].forEach(function(name) {
    api[name] = function(url, data, config) {
      return api(extend(config || {}, {
        method: name,
        url: api.config.host + url,
        data: data
      }));
    };
  });

  return api;

}]);
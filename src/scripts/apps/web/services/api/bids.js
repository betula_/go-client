
module.run(['$api', '$apiSanitizer', function($api, $apiSanitizer) {

  $api.bid = function() {
    return $apiSanitizer.single($api.post('/bid'));
  };

  $api.bids = function() {
    return $apiSanitizer.multiple($api.get('/bids'));
  };

}]);
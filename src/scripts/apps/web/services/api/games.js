
module.run(['$api', '$apiSanitizer', function($api, $apiSanitizer) {

  $api.game = function(bid) {
    return $apiSanitizer.single($api.post('/game', {
      bid: parseInt(bid)
    }));
  };

  $api.game.active = function() {
    return $apiSanitizer.single($api.get('/game/active'));
  };

  $api.game.active.step = function(row, col) {
    return $apiSanitizer.single($api.post('/game/active/step', {
      row: parseInt(row),
      col: parseInt(col)
    }));
  };

  $api.game.active.step.pass = function() {
    return $apiSanitizer.single($api.post('/game/active/step/pass'));
  };

  $api.game.active.quit = function() {
    return $apiSanitizer.single($api.post('/game/active/quit'));
  };

  $api.games = function() {
    return $apiSanitizer.multiple($api.get('/games'));
  };

}]);
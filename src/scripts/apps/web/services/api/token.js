
module.run(['$api', '$apiSanitizer', function($api, $apiSanitizer) {

  $api.token = function(email, password) {
    return $apiSanitizer.single($api.post('/token', {
      email: email,
      password: password
    }));
  };

}]);
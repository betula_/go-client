
module.run(['$api', '$apiSanitizer', function($api, $apiSanitizer) {

  $api.user = function(email, password) {
    return $apiSanitizer.single($api.post('/user', {
      email: email,
      password: password
    }));
  };

  $api.user.get = function() {
    return $apiSanitizer.single($api.get('/user'));
  };

  $api.user.roles = function() {
    return $apiSanitizer.multiple($api.get('/user/roles'));
  };


}]);

module.factory('$apiSanitizer', ['$q', function($q) {

  var extend = angular.extend;
  var copy = angular.copy;

  return {

    single: function(promise, formatter) {
      var result = {};
      result.$promise = promise.then(function(data) {
        extend(result, data.data);
        if (formatter) {
          return formatter(result);
        }
        return result;
      }, function(error) {
        return $q.reject(error.data);
      });
      return result;
    },

    multiple: function(promise, formatter) {
      var result = [];
      result.$promise = promise.then(function(data) {
        copy(data.data, result);
        if (formatter) {
          return formatter(result);
        }
        return result;
      }, function(error) {
        return $q.reject(error.data);
      });
      return result;
    },

    paging: function(promise, formatter) {
      var result = [];
      result.$promise = promise.then(function(data) {
        copy(data.data.data, result);
        result.$paging = data.data.paging;
        if (formatter) {
          return formatter(result);
        }
        return result;
      }, function(error) {
        return $q.reject(error.data);
      });
      return result;
    }

  };

}]);

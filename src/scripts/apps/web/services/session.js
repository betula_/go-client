
module.factory('$session', ['$sessionStore', '$api', '$q', function($sessionStore, $api, $q) {
  var copy = angular.copy;

  return {

    performed: false,
    roles: [],
    token: null,
    user: {},
    authorized: false,

    resolve: function(data) {
      if (data) {
        this.authorized = false;
        this.roles.length = 0;
        this.roles.isUser = false;
        this.token = null;
        copy({}, this.user);
      }
      if ((data || {}).token) {
        this.token = data.token;
      }
      if ((data || {}).user) {
        copy(data.user, this.user);
      }
      if ((data || {}).roles) {
        copy(data.roles, this.roles);
        this.authorized = this.roles.length;
        this.roles.isUser = this.roles.indexOf('user') >= 0;
      }
      this.performed = true;
    },

    start: function(email, password) {
      var done = $q.defer();

      var self = this;
      $api.token(email, password).$promise.then(function(data) {
        $sessionStore.set(data);

        $api.user.roles().$promise.then(function(roles) {
          var session = $sessionStore.get() || {};
          self.resolve({
            token: session.token,
            user: session.user,
            roles: roles.roles
          });
          done.resolve(self);
        }, function(data) {
          self.resolve();
          done.reject(data);
        });

      }, function(data) {
        self.resolve();
        done.reject(data);
      });

      return done.promise;
    },

    load: function() {
      var done = $q.defer();

      var session = $sessionStore.get();
      if (session) {
        var self = this;
        $api.user.roles().$promise.then(function(roles) {
          self.resolve({
            token: session.token,
            user: session.user,
            roles: roles.roles
          });
          done.resolve(self);
        }, function(data) {
          self.resolve();
          done.reject(data);
        });
      }
      else {
        this.resolve();
        done.reject();
      }

      return done.promise;
    },

    refresh: function() {
      var done = $q.defer();
      var resolve = function() {
        done.resolve(this);
      }.bind(this);
      this.load().then(resolve, resolve);
      return done.promise;
    },

    perform: function() {
      var done = $q.defer();

      var resolve = function() {
        done.resolve(this);
        delete this.$$performPromise;
      }.bind(this);
      if (this.performed) {
        resolve();
      }
      else {
        if (this.$$performPromise) {
          return this.$$performPromise;
        }
        this.$$performPromise = done.promise;
        this.load().then(resolve, resolve);
      }
      return done.promise;
    },

    destroy: function() {
      $sessionStore.clear();
      this.resolve({});
    }


  };

}]);

module.factory('$sessionStore', ['$cookieStore', function($cookieStore) {
  var copy = angular.copy;
  var store = $cookieStore;
  var key = '$$__session';

  return {
    get: function() {
      try {
        return store.get(key) || null;
      }
      catch(e) {
        store.put(key, null);
        return null;
      }
    },

    set: function(token) {
      var data = {};
      for (var i in token) {
        if (token.hasOwnProperty(i) && i[0] != '$') {
          data[i] = copy(token[i]);
        }
      }
      store.put(key, data);
    },

    clear: function() {
      store.remove(key);
    }
  };

}]);
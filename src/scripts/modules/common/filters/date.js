
module.filter('date', [function() {

  return function(date, format){
    var dt;
    if (date) {
      try {
        dt = new Date(date);
      }
      catch (e) {
        return '';
      }
    }
    else {
      return '';
    }

    if (!format) {
      return formatter(dt, 'd.m.Y');
    }
    return formatter(dt, format);
  };


  function formatter (date, format) {
    var dt = new Date(date);
    var f = {

      // Seconds
      s: function() {
        return ('0' + dt.getSeconds()).substr(-2);
      },

      // Minutes
      i: function() {
        return ('0' + dt.getMinutes()).substr(-2);
      },

      // Hours
      H: function() {
        return ('0' + dt.getHours()).substr(-2);
      },

      // Day
      j: function() {
        return dt.getDate();
      },
      d: function() {
        return ('0' + this.j()).substr(-2);
      },

      // Month
      n: function() {
        return dt.getMonth() + 1;
      },
      m: function() {
        return ('0' + this.n()).substr(-2);
      },

      // Year
      Y: function() {
        return dt.getFullYear();
      },
      y: function() {
        return String.prototype.substr.call(this.Y(), -2);
      }

    };

    return format.replace(/[\\]?([a-zA-Z])/g, function(m0, m){
      if(m0 != m || !f.hasOwnProperty(m)) {
        return m;
      }
      else {
        return f[m]();
      }
    });

  }

}]);
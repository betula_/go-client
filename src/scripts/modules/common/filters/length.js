
module.filter('length', [function() {

  return function(data){
    return data ? (data.length || 0) : 0;
  };

}]);
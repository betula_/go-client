
module.factory('$loadingIndicator', ['$rootScope', function($rootScope) {
  var counter = 0;
  var loading = {

    isReady: true,
    isLoading: false,

    start: function() {
      if (counter++ === 0) {
        loading.isLoading = true;
        loading.isReady = false;
        if (!$rootScope.$$phase) {
          $rootScope.$apply();
        }
      }
    },

    stop: function() {
      if (--counter === 0) {
        loading.isReady = true;
        loading.isLoading = false;
        if (!$rootScope.$$phase) {
          $rootScope.$apply();
        }
      }
    }

  };
  return loading;
}]);

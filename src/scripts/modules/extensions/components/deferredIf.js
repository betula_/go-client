
module.directive('deferredIf', ['$timeout', function($timeout) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			var promise;
			scope.$watch(attrs.deferredIf, function(value) {
				if (value) {
					$timeout.cancel(promise);
					promise = $timeout(function() {
						if (scope.$eval(attrs.deferredIf)) {
							scope.$eval(attrs.deferredIfExpression);
						}
					}, attrs.deferredIfTimeout);
				}
			});
			scope.$on('$destroy', function() {
				$timeout.cancel(promise);
			});
		}
	}
}]);
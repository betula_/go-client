
module.directive('formAjax', ['$http', 'jQuery', function($http, jQuery) {
	return {
		restrict: 'A',
		require: ['form', 'formAjax'],
		controller: ['$scope', '$element', '$attrs', '$location', function($scope, $element, $attrs, $location) {
			var form = $element.controller('form');

			this.getFormData = function() {
				var data = {};
				data.__location = $location.absUrl();
				if ($attrs.formAjaxData) {
					data = Object($scope.$eval($attrs.formAjaxData));
				}
				else {
					Object.keys(form).forEach(function(name) {
						if (Object(form[name]).hasOwnProperty('$modelValue')) {
							data[name] = form[name].$modelValue;
						}
					});
				}
				if ($attrs.formAjaxAppend) {
					var appendedData = Object($scope.$eval($attrs.formAjaxAppend));
					Object.keys(appendedData).forEach(function(name) {
						data[name] = appendedData[name];
					});
				}
				if ($attrs.formAjaxRequest) {
					var requestedData = $scope.$eval($attrs.formAjaxRequest, {
						$data: data
					});
					if (requestedData) {
						data = requestedData;
					}
				}
				return data;
			};
		}],
		link: function(scope, element, attrs, ctrls) {
			var form = ctrls[0];
			var formAjax = ctrls[1];
			var getFormData = formAjax.getFormData;

			var pending = {
				counter: 0,
				start: function() {
					this.counter ++;
					form.$submitting = true;
				},
				finish: function() {
					if (-- this.counter == 0) {
						form.$submitting = false;
					}
				}
			};

			element.on('submit', function(event, options) {
				options = options || {};
				event.preventDefault();
				form.$submitted = true;

				if (attrs.formAjaxSubmit) {
					scope.$eval(attrs.formAjaxSubmit, {
						$form: form
					});
				}

				if (form.$invalid) {
					if (!scope.$root.$$phase) scope.$digest();
					return;
				}

				var url = attrs.action || attrs.formAjax;
				var method = String(attrs.method || attrs.formAjaxMethod).toLowerCase();
				if (['get', 'post', 'put', 'delete', 'head', 'jsonp'].indexOf(method) == -1) method = 'post';

				var data = options.data || getFormData();
				var promise;
				if (attrs.formAjaxPerform) {
					promise = scope.$eval(attrs.formAjaxPerform, {
						$method: method,
						$url: url,
						$data: data
					});
				}
				else {
					var config = {
						url: url,
						method: method
					};
					if (method == 'get') {
						config.params = data;
					} else {
						config.data = jQuery.param(data);
						config.headers = {
							'Content-Type': 'application/x-www-form-urlencoded'
						};
					}
					promise = $http(config);
				}

				if (attrs.formAjaxResponse) {
					var newPromise = scope.$eval(attrs.formAjaxResponse, {
						$method: method,
						$action: url,
						$request: data,
						$promise: promise
					});
					if (newPromise) {
						promise = newPromise;
					}
				}

				pending.start();
				promise.then(function(response) {
					pending.finish();
					form.$isSubmitError = false;
					form.$isSubmitSuccess = true;

					if (attrs.formAjaxSuccess) {
						scope.$eval(attrs.formAjaxSuccess, {
							$request: data,
							$response: response,
							$data: response.data
						});
					}
				}, function(response) {
					pending.finish();
					form.$isSubmitError = true;
					form.$isSubmitSuccess = false;

					if (attrs.formAjaxError) {
						scope.$eval(attrs.formAjaxError, {
							$request: data,
							$response: response,
							$data: response.data
						});
					}

					if (form.$submitting) return;

					if (response.data && response.data.errors && response.data.errors.length) {
						response.data.errors.forEach(function(error) {
							var key = error.path;
							var name = error.code;
							if (Object(form[key]).hasOwnProperty('$modelValue')) {
								var ctrl = form[key];
								if (ctrl.$modelValue != data[key]) return;

								ctrl.$setValidity(name, false);

								var validator = function(value) {
									ctrl.$setValidity(name, true);
									var i;
									for (i = 0; i < ctrl.$parsers.length; ) {
										if (ctrl.$parsers[i] === validator) {
											ctrl.$parsers.splice(i, 1);
										} else ++i;
									}
									for (i = 0; i < ctrl.$formatters.length; ) {
										if (ctrl.$formatters[i] === validator) {
											ctrl.$formatters.splice(i, 1);
										} else ++i;
									}
									return value;
								};
								ctrl.$parsers.push(validator); // TODO: It should be executed first
								ctrl.$formatters.push(validator); // TODO: It should be executed first
							}
						});
					}

				});
				if (!scope.$root.$$phase) scope.$digest();
			});
		}
	}
}]);

module.directive('imgLoading', [function() {
	return {
		restrict: 'A',
		compile: function() {
			return {
				pre: function(scope, element, attrs) {
					element.addClass('ng-loading');
					attrs.$observe('src', function() {
						element.removeClass('ng-loaded');
					});
					element.on('load', function() {
						element.addClass('ng-loaded');
					});
					element.on('error', function() {
						// TODO Check "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw=="
						element.attr('src', 'data:image/gif;base64,R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAAAICTAEAOw==');
					});
				}
			}
		}
	}
}]);